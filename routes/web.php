<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('test', 'HomeController@test');

Route::post('login', 'HomeController@login');
Route::get('logout', 'HomeController@logout');
// Auth::routes();

Route::get('audit', 'HomeController@audit');
Route::get('level1_copy/{id}', 'HomeController@level1_copy');
Route::get('level2_copy/{id}', 'HomeController@level2_copy');
Route::get('level3_copy/{id}', 'HomeController@level3_copy');

Route::resource('iso_category_level1', 'IsoCategoryLevel1Controller');
Route::resource('iso_category_level2', 'IsoCategoryLevel2Controller');
Route::resource('iso_category_level3', 'IsoCategoryLevel3Controller');
Route::resource('iso_category_level4', 'IsoCategoryLevel4Controller');
Route::resource('document_review', 'DocumentReviewController');

Route::get('questioner', 'AnswerController@questioner');
Route::get('question/{level1}/{level2}/{level3}/{level4}', 'AnswerController@question4');
Route::get('question/{level1}/{level2}/{level3}', 'AnswerController@question3');
Route::get('question/{level1}/{level2}', 'AnswerController@question2');
Route::get('question/{level1}', 'AnswerController@question1');
Route::get('question', 'AnswerController@question');
Route::get('question_document', 'AnswerController@question_document');

Route::post('input_respondent', 'AnswerController@input_respondent');
Route::post('input_question_document', 'AnswerController@input_question_document');
Route::post('next_question', 'AnswerController@next_question');

Route::post('update_sort1', 'HomeController@update_sort1');
Route::post('update_sort2', 'HomeController@update_sort2');
Route::post('update_sort3', 'HomeController@update_sort3');
Route::post('update_sort4', 'HomeController@update_sort4');
Route::post('update_sort_document_review', 'HomeController@update_sort_document_review');

Route::get('judgement_scale_level1', 'AnalysisController@judgement_scale_level1');
Route::get('judgement_scale_level2', 'AnalysisController@judgement_scale_level2');
Route::get('judgement_scale_level3', 'AnalysisController@judgement_scale_level3');
Route::get('judgement_scale_level4', 'AnalysisController@judgement_scale_level4');

Route::post('analysis_level1s', 'AnalysisController@analysis_level1');
Route::post('analysis_level2s', 'AnalysisController@analysis_level2');
Route::post('analysis_level3s', 'AnalysisController@analysis_level3');
Route::post('analysis_level4s', 'AnalysisController@analysis_level4');

Route::get('result', 'HomeController@result');

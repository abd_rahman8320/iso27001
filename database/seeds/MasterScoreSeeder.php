<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\MasterScore;

class MasterScoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_scores')->truncate();
        MasterScore::insert([
            [
                'score' => 9,
                'keterangan' => 'Mutlak sangat penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 8,
                'keterangan' => 'Mendekati mutlak sangat penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 7,
                'keterangan' => 'Sangat penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 6,
                'keterangan' => 'Mendekati sangat penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 5,
                'keterangan' => 'Lebih penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 4,
                'keterangan' => 'Mendekati lebih penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 3,
                'keterangan' => 'Sedikit lebih penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 2,
                'keterangan' => 'Mendekati sedikit lebih penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1,
                'keterangan' => 'Sama penting dengan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1/2,
                'keterangan' => 'Mendekati sedikit kurang penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1/3,
                'keterangan' => 'Sedikit kurang penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1/4,
                'keterangan' => 'Mendekati kurang penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1/5,
                'keterangan' => 'Kurang penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1/6,
                'keterangan' => 'Mendekati sangat tidak penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1/7,
                'keterangan' => 'Sangat tidak penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1/8,
                'keterangan' => 'Mendekati mutlak sangat tidak penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'score' => 1/9,
                'keterangan' => 'Mutlak sangat tidak penting dari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}

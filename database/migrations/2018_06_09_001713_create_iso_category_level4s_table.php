<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsoCategoryLevel4sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iso_category_level4', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_level3_id');
            $table->string('code');
            $table->string('title');
            $table->float('max_score', 20, 17)->default(0);
            $table->float('sum', 20, 17)->default(0);
            $table->boolean('is_visible');
            $table->integer('sort');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iso_category_level4s');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsistencyRatioLevel1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consistency_ratio_level1s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('n');
            $table->float('eigen_max', 20, 17)->default(0);
            $table->float('ci', 20, 17)->default(0);
            $table->float('cr', 20, 17)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consistency_ratio_level1s');
    }
}

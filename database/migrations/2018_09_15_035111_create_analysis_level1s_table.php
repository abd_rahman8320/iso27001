<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisLevel1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_level1s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('first_criteria');
            $table->integer('second_criteria');
            $table->float('scale', 20, 17)->default(0);
            $table->float('result', 20, 17)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_level1s');
    }
}

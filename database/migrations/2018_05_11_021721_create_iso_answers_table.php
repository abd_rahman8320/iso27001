<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsoAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iso_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('respondent_id');
            $table->integer('question_id');
            $table->float('max_score', 20, 17);
            $table->float('score', 20, 17);
            $table->integer('status')->comment('0 - Tidak Dilaksanakan, 1 - Dalam Perencanaan, 2 - Sudah Dilaksanakan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iso_answers');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisLevel3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_level3s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('first_criteria');
            $table->integer('second_criteria');
            $table->integer('parent_id');
            $table->float('scale', 20, 17)->default(0);
            $table->float('result', 20, 17)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_level3s');
    }
}

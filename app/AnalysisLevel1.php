<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalysisLevel1 extends Model
{
    protected $table = 'analysis_level1s';

    protected $fillable = [
        'first_criteria',
        'second_criteria',
        'scale',
        'result'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsistencyRatioLevel2 extends Model
{
    protected $table = 'consistency_ratio_level2s';

    protected $fillable = [
        'parent_id',
        'n',
        'eigen_max',
        'ci',
        'cr'
    ];
}

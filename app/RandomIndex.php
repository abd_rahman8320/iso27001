<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RandomIndex extends Model
{
    protected $table = 'random_indices';

    protected $fillable = [
        'n',
        'ri'
    ];
}

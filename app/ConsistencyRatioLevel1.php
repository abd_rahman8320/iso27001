<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsistencyRatioLevel1 extends Model
{
    protected $table = 'consistency_ratio_level1s';

    protected $fillable = [
        'n',
        'eigen_max',
        'ci',
        'cr'
    ];
}

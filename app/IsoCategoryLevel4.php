<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IsoCategoryLevel4 extends Model
{
    protected $table = 'iso_category_level4';

    protected $fillable = [
        'category_level3_id',
        'code',
        'title',
        'sum',
        'max_score',
        'is_visible',
        'sort'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IsoCategoryLevel3 extends Model
{
    protected $table = 'iso_category_level3';

    protected $fillable = [
        'category_level2_id',
        'code',
        'title',
        'sum',
        'max_score',
        'is_visible',
        'sort'
    ];
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\ConsistencyRatioLevel1;
use App\ConsistencyRatioLevel2;
use App\ConsistencyRatioLevel3;
use App\ConsistencyRatioLevel4;

class AnalysisController extends Controller
{
    public function judgement_scale_level1(Request $request)
    {
        $get = DB::table('iso_category_level1')
        ->get();
        $array = [];
        $i = 0;
        foreach ($get as $row) {
            array_push($array, $row->id);
            $get_after[$i] = DB::table('iso_category_level1')
            ->where(function($q) use($array){
                foreach ($array as $value) {
                    $q = $q->where('id', '!=', $value);
                }
            })
            ->orderBy('sort', 'asc')
            ->get();
            $i++;
        }
        $scores = DB::table('master_scores')
        ->orderBy('score', 'desc')
        ->get();

        if ($get->isEmpty()) {
            return Redirect::to('iso_category_level1')->with('error', 'Data Kosong');
        }else {
            return view('judgement_scale_level1')
            ->with('x', $get)
            ->with('y', $get_after)
            ->with('i', $i)
            ->with('scores', $scores);
        }
    }

    public function judgement_scale_level2(Request $request)
    {
        $get = DB::table('iso_category_level2')
        ->where('category_level1_id', $request->input('id'))
        ->get();
        $array = [];
        $i = 0;
        foreach ($get as $row) {
            array_push($array, $row->id);
            $get_after[$i] = DB::table('iso_category_level2')
            ->where('category_level1_id', $request->input('id'))
            ->where(function($q) use($array){
                foreach ($array as $value) {
                    $q = $q->where('id', '!=', $value);
                }
            })
            ->orderBy('sort', 'asc')
            ->get();
            $i++;
        }
        $scores = DB::table('master_scores')
        ->orderBy('score', 'desc')
        ->get();

        if ($get->isEmpty()) {
            return Redirect::to('iso_category_level2')->with('error', 'Data Kosong');
        }else {
            return view('judgement_scale_level2')
            ->with('x', $get)
            ->with('y', $get_after)
            ->with('i', $i)
            ->with('scores', $scores)
            ->with('id', $request->input('id'));
        }
    }

    public function judgement_scale_level3(Request $request)
    {
        $get = DB::table('iso_category_level3')
        ->where('category_level2_id', $request->input('id'))
        ->get();
        $array = [];
        $i = 0;
        foreach ($get as $row) {
            array_push($array, $row->id);
            $get_after[$i] = DB::table('iso_category_level3')
            ->where('category_level2_id', $request->input('id'))
            ->where(function($q) use($array){
                foreach ($array as $value) {
                    $q = $q->where('id', '!=', $value);
                }
            })
            ->orderBy('sort', 'asc')
            ->get();
            $i++;
        }
        $scores = DB::table('master_scores')
        ->orderBy('score', 'desc')
        ->get();

        if ($get->isEmpty()) {
            return Redirect::to('iso_category_level3')->with('error', 'Data Kosong');
        }else {
            return view('judgement_scale_level3')
            ->with('x', $get)
            ->with('y', $get_after)
            ->with('i', $i)
            ->with('scores', $scores)
            ->with('id', $request->input('id'));
        }
    }

    public function judgement_scale_level4(Request $request)
    {
        $get = DB::table('iso_category_level4')
        ->where('category_level3_id', $request->input('id'))
        ->get();
        $array = [];
        $i = 0;
        foreach ($get as $row) {
            array_push($array, $row->id);
            $get_after[$i] = DB::table('iso_category_level4')
            ->where('category_level3_id', $request->input('id'))
            ->where(function($q) use($array){
                foreach ($array as $value) {
                    $q = $q->where('id', '!=', $value);
                }
            })
            ->orderBy('sort', 'asc')
            ->get();
            $i++;
        }
        $scores = DB::table('master_scores')
        ->orderBy('score', 'desc')
        ->get();

        if ($get->isEmpty()) {
            return Redirect::to('iso_category_level4')->with('error', 'Data Kosong');
        }else {
            return view('judgement_scale_level4')
            ->with('x', $get)
            ->with('y', $get_after)
            ->with('i', $i)
            ->with('scores', $scores)
            ->with('id', $request->input('id'));
        }
    }

    public function analysis_level1(Request $request)
    {
        $x = $request->input('x');
        if ($x == null) {
            $count_x = 0;
        }else {
            $count_x = count($x);
        }
        $y = $request->input('y');
        $nilai = $request->input('nilai');

        $truncate = DB::table('analysis_level1s')->truncate();

        for ($i=0; $i < $count_x; $i++) {
            $insert_scale = DB::table('analysis_level1s')
            ->insert([
                [
                    'first_criteria' => $x[$i],
                    'second_criteria' => $y[$i],
                    'scale' => $nilai[$i]
                ],
                [
                    'first_criteria' => $y[$i],
                    'second_criteria' => $x[$i],
                    'scale' => 1 / $nilai[$i]
                ]
            ]);
        }

        $get = DB::table('iso_category_level1')->get();
        foreach ($get as $data) {
            $insert_scale1 = DB::table('analysis_level1s')
            ->insert([
                'first_criteria' => $data->id,
                'second_criteria' => $data->id,
                'scale' => 1
            ]);
        }

        $select_scale = DB::table('analysis_level1s')
        ->groupBy('second_criteria')
        ->selectRaw('SUM(scale) as sum, second_criteria')
        ->get();

        foreach ($select_scale as $select) {
            $insert_sum = DB::table('iso_category_level1')
            ->where('id', $select->second_criteria)
            ->update([
                'sum' => $select->sum
            ]);
        }

        $select_analysis = DB::table('analysis_level1s')
        ->get();

        foreach ($select_analysis as $row) {
            $select_sum = DB::table('iso_category_level1')
            ->where('id', $row->second_criteria)
            ->first();
            $result = $row->scale / $select_sum->sum;

            $update_result = DB::table('analysis_level1s')
            ->where('id', $row->id)
            ->update([
                'result' => $result
            ]);
        }

        $get_analysis = DB::table("analysis_level1s")
        ->groupBy('first_criteria')
        ->selectRaw('SUM(result) as result, first_criteria')
        ->get();
        $count_category = count($get);
        foreach ($get_analysis as $data2) {
            $update_max_score = DB::table('iso_category_level1')
            ->where('id', $data2->first_criteria)
            ->update([
                'max_score' => $data2->result / $count_category
            ]);
        }

        $n = $count_category;
        if ($n > 2) {
            $eigen_max = DB::table('iso_category_level1')
            ->selectRaw('SUM(max_score * sum) as eigen_max')
            ->first();
            $ci = ($eigen_max->eigen_max - $n) / ($n - 1);
            $ri = DB::table('random_indices')
            ->where('n', $n)
            ->sum('ri');
            $cr = $ci / $ri;
        }else {
            $eigen_max = DB::table('iso_category_level1')
            ->selectRaw('SUM(max_score * sum) as eigen_max')
            ->first();
            $ci = 0;
            $ri = 0;
            $cr = 0;
        }

        ConsistencyRatioLevel1::truncate();
        $data = new ConsistencyRatioLevel1;
        $data->eigen_max = $eigen_max->eigen_max;
        $data->n = $n;
        $data->ci = $ci;
        $data->cr = $cr;
        $data->save();

        return Redirect::to('iso_category_level1')->with('success', 'Bobot Berhasil Dihitung');
    }

    public function analysis_level2(Request $request)
    {
        $x = $request->input('x');
        if ($x == null) {
            $count_x = 0;
        }else {
            $count_x = count($x);
        }
        $y = $request->input('y');
        $nilai = $request->input('nilai');
        $parent_id = $request->input('parent_id');

        $delete = DB::table('analysis_level2s')
        ->where('parent_id', $parent_id)
        ->delete();

        for ($i=0; $i < $count_x; $i++) {
            $insert_scale = DB::table('analysis_level2s')
            ->insert([
                [
                    'first_criteria' => $x[$i],
                    'second_criteria' => $y[$i],
                    'scale' => $nilai[$i],
                    'parent_id' => $parent_id
                ],
                [
                    'first_criteria' => $y[$i],
                    'second_criteria' => $x[$i],
                    'scale' => 1 / $nilai[$i],
                    'parent_id' => $parent_id
                ]
            ]);
        }

        $get = DB::table('iso_category_level2')
        ->where('category_level1_id', $parent_id)
        ->get();
        foreach ($get as $data) {
            $insert_scale1 = DB::table('analysis_level2s')
            ->insert([
                'first_criteria' => $data->id,
                'second_criteria' => $data->id,
                'scale' => 1,
                'parent_id' => $parent_id
            ]);
        }

        $select_scale = DB::table('analysis_level2s')
        ->where('parent_id', $parent_id)
        ->groupBy('second_criteria')
        ->selectRaw('SUM(scale) as sum, second_criteria')
        ->get();

        foreach ($select_scale as $select) {
            $insert_sum = DB::table('iso_category_level2')
            ->where('id', $select->second_criteria)
            ->update([
                'sum' => $select->sum
            ]);
        }

        $select_analysis = DB::table('analysis_level2s')
        ->where('parent_id', $parent_id)
        ->get();

        foreach ($select_analysis as $row) {
            $select_sum = DB::table('iso_category_level2')
            ->where('id', $row->second_criteria)
            ->first();
            $result = $row->scale / $select_sum->sum;

            $update_result = DB::table('analysis_level2s')
            ->where('id', $row->id)
            ->update([
                'result' => $result
            ]);
        }

        $get_analysis = DB::table("analysis_level2s")
        ->where('parent_id', $parent_id)
        ->groupBy('first_criteria')
        ->selectRaw('SUM(result) as result, first_criteria')
        ->get();
        $count_category = count($get);
        foreach ($get_analysis as $data2) {
            $update_max_score = DB::table('iso_category_level2')
            ->where('id', $data2->first_criteria)
            ->update([
                'max_score' => $data2->result / $count_category
            ]);
        }

        $n = $count_category;
        if ($n > 2) {
            $eigen_max = DB::table('iso_category_level2')
            ->selectRaw('SUM(max_score * sum) as eigen_max')
            ->where('category_level1_id', $parent_id)
            ->first();
            $ci = ($eigen_max->eigen_max - $n) / ($n - 1);
            $ri = DB::table('random_indices')
            ->where('n', $n)
            ->sum('ri');
            $cr = $ci / $ri;
        }else {
            $eigen_max = DB::table('iso_category_level2')
            ->selectRaw('SUM(max_score * sum) as eigen_max')
            ->where('category_level1_id', $parent_id)
            ->first();
            $ci = 0;
            $ri = 0;
            $cr = 0;
        }

        ConsistencyRatioLevel2::where('parent_id', $parent_id)->delete();
        $data = new ConsistencyRatioLevel2;
        $data->eigen_max = $eigen_max->eigen_max;
        $data->n = $n;
        $data->ci = $ci;
        $data->cr = $cr;
        $data->parent_id = $parent_id;
        $data->save();

        return Redirect::to('iso_category_level2?id='.$parent_id)->with('success', 'Bobot Berhasil Dihitung');
    }

    public function analysis_level3(Request $request)
    {
        $x = $request->input('x');
        if ($x == null) {
            $count_x = 0;
        }else {
            $count_x = count($x);
        }
        $y = $request->input('y');
        $nilai = $request->input('nilai');
        $parent_id = $request->input('parent_id');

        $delete = DB::table('analysis_level3s')
        ->where('parent_id', $parent_id)
        ->delete();

        for ($i=0; $i < $count_x; $i++) {
            $insert_scale = DB::table('analysis_level3s')
            ->insert([
                [
                    'first_criteria' => $x[$i],
                    'second_criteria' => $y[$i],
                    'scale' => $nilai[$i],
                    'parent_id' => $parent_id
                ],
                [
                    'first_criteria' => $y[$i],
                    'second_criteria' => $x[$i],
                    'scale' => 1 / $nilai[$i],
                    'parent_id' => $parent_id
                ]
            ]);
        }

        $get = DB::table('iso_category_level3')
        ->where('category_level2_id', $parent_id)
        ->get();
        foreach ($get as $data) {
            $insert_scale1 = DB::table('analysis_level3s')
            ->insert([
                'first_criteria' => $data->id,
                'second_criteria' => $data->id,
                'scale' => 1,
                'parent_id' => $parent_id
            ]);
        }

        $select_scale = DB::table('analysis_level3s')
        ->where('parent_id', $parent_id)
        ->groupBy('second_criteria')
        ->selectRaw('SUM(scale) as sum, second_criteria')
        ->get();

        foreach ($select_scale as $select) {
            $insert_sum = DB::table('iso_category_level3')
            ->where('id', $select->second_criteria)
            ->update([
                'sum' => $select->sum
            ]);
        }

        $select_analysis = DB::table('analysis_level3s')
        ->where('parent_id', $parent_id)
        ->get();

        foreach ($select_analysis as $row) {
            $select_sum = DB::table('iso_category_level3')
            ->where('id', $row->second_criteria)
            ->first();
            $result = $row->scale / $select_sum->sum;

            $update_result = DB::table('analysis_level3s')
            ->where('id', $row->id)
            ->update([
                'result' => $result
            ]);
        }

        $get_analysis = DB::table("analysis_level3s")
        ->where('parent_id', $parent_id)
        ->groupBy('first_criteria')
        ->selectRaw('SUM(result) as result, first_criteria')
        ->get();
        $count_category = count($get);
        foreach ($get_analysis as $data2) {
            $update_max_score = DB::table('iso_category_level3')
            ->where('id', $data2->first_criteria)
            ->update([
                'max_score' => $data2->result / $count_category
            ]);
        }

        $n = $count_category;
        if ($n > 2) {
            $eigen_max = DB::table('iso_category_level3')
            ->selectRaw('SUM(max_score * sum) as eigen_max')
            ->where('category_level2_id', $parent_id)
            ->first();
            $ci = ($eigen_max->eigen_max - $n) / ($n - 1);
            $ri = DB::table('random_indices')
            ->where('n', $n)
            ->sum('ri');
            $cr = $ci / $ri;
        }else {
            $eigen_max = DB::table('iso_category_level3')
            ->selectRaw('SUM(max_score * sum) as eigen_max')
            ->where('category_level2_id', $parent_id)
            ->first();
            $ci = 0;
            $ri = 0;
            $cr = 0;
        }

        ConsistencyRatioLevel3::where('parent_id', $parent_id)->delete();
        $data = new ConsistencyRatioLevel3;
        $data->eigen_max = $eigen_max->eigen_max;
        $data->n = $n;
        $data->ci = $ci;
        $data->cr = $cr;
        $data->parent_id = $parent_id;
        $data->save();

        return Redirect::to('iso_category_level3?id='.$parent_id)->with('success', 'Bobot Berhasil Dihitung');
    }

    public function analysis_level4(Request $request)
    {
        $x = $request->input('x');
        if ($x == null) {
            $count_x = 0;
        }else {
            $count_x = count($x);
        }
        $y = $request->input('y');
        $nilai = $request->input('nilai');
        $parent_id = $request->input('parent_id');

        $delete = DB::table('analysis_level4s')
        ->where('parent_id', $parent_id)
        ->delete();

        for ($i=0; $i < $count_x; $i++) {
            $insert_scale = DB::table('analysis_level4s')
            ->insert([
                [
                    'first_criteria' => $x[$i],
                    'second_criteria' => $y[$i],
                    'scale' => $nilai[$i],
                    'parent_id' => $parent_id
                ],
                [
                    'first_criteria' => $y[$i],
                    'second_criteria' => $x[$i],
                    'scale' => 1 / $nilai[$i],
                    'parent_id' => $parent_id
                ]
            ]);
        }

        $get = DB::table('iso_category_level4')
        ->where('category_level3_id', $parent_id)
        ->get();
        foreach ($get as $data) {
            $insert_scale1 = DB::table('analysis_level4s')
            ->insert([
                'first_criteria' => $data->id,
                'second_criteria' => $data->id,
                'scale' => 1,
                'parent_id' => $parent_id
            ]);
        }

        $select_scale = DB::table('analysis_level4s')
        ->where('parent_id', $parent_id)
        ->groupBy('second_criteria')
        ->selectRaw('SUM(scale) as sum, second_criteria')
        ->get();

        foreach ($select_scale as $select) {
            $insert_sum = DB::table('iso_category_level4')
            ->where('id', $select->second_criteria)
            ->update([
                'sum' => $select->sum
            ]);
        }

        $select_analysis = DB::table('analysis_level4s')
        ->where('parent_id', $parent_id)
        ->get();

        foreach ($select_analysis as $row) {
            $select_sum = DB::table('iso_category_level4')
            ->where('id', $row->second_criteria)
            ->first();
            $result = $row->scale / $select_sum->sum;

            $update_result = DB::table('analysis_level4s')
            ->where('id', $row->id)
            ->update([
                'result' => $result
            ]);
        }

        $get_analysis = DB::table("analysis_level4s")
        ->where('parent_id', $parent_id)
        ->groupBy('first_criteria')
        ->selectRaw('SUM(result) as result, first_criteria')
        ->get();
        $count_category = count($get);
        foreach ($get_analysis as $data2) {
            $update_max_score = DB::table('iso_category_level4')
            ->where('id', $data2->first_criteria)
            ->update([
                'max_score' => $data2->result / $count_category
            ]);
        }

        $n = $count_category;
        if ($n > 2) {
            $eigen_max = DB::table('iso_category_level4')
            ->selectRaw('SUM(max_score * sum) as eigen_max')
            ->where('category_level3_id', $parent_id)
            ->first();
            $ci = ($eigen_max->eigen_max - $n) / ($n - 1);
            $ri = DB::table('random_indices')
            ->where('n', $n)
            ->sum('ri');
            $cr = $ci / $ri;
        }else {
            $eigen_max = DB::table('iso_category_level4')
            ->selectRaw('SUM(max_score * sum) as eigen_max')
            ->where('category_level3_id', $parent_id)
            ->first();
            $ci = 0;
            $ri = 0;
            $cr = 0;
        }

        ConsistencyRatioLevel4::where('parent_id', $parent_id)->delete();
        $data = new ConsistencyRatioLevel4;
        $data->eigen_max = $eigen_max->eigen_max;
        $data->n = $n;
        $data->ci = $ci;
        $data->cr = $cr;
        $data->parent_id = $parent_id;
        $data->save();

        return Redirect::to('iso_category_level4?id='.$parent_id)->with('success', 'Bobot Berhasil Dihitung');
    }
}

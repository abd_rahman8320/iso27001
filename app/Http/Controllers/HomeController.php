<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use DB;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
        // return Redirect::to('iso_category_level1');
    }

    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $check_credential = DB::table('respondents')
        ->where('name', $username)
        ->where('password', $password)
        ->first();

        if ($check_credential) {
            Session::put('respondent_id', $check_credential->id);
            Session::put('respondent_is_admin', $check_credential->is_admin);
            if ($check_credential->is_admin == 1) {
                return Redirect::to('iso_category_level1');
            }else {
                return Redirect::to('question_document');
            }

        }else {
            return Redirect::to('/');
        }
    }

    public function logout()
    {
        session_unset();
        return Redirect::to('/');
    }

    public function result()
    {
        if (Session::has('respondent_id')) {
            $result = DB::table('iso_answers')
            ->where('respondent_id', Session::get('respondent_id'))
            ->selectRaw('
                SUM(max_score * score) as result
            ')
            ->first();

            $not_achive = 1 - $result->result;

            if ($result->result >= 0.9) {
                $category = 'Sangat Siap';
            }elseif ($result->result < 0.9 && $result->result >= 0.7) {
                $category = 'Cukup Siap';
            }elseif ($result->result < 0.7 && $result->result >= 0.6) {
                $category = 'Kurang Siap';
            }else {
                $category = 'Sangat Tidak Siap';
            }

            $answers_no = DB::select('select a.* from iso_category_level4 a
            where id not in(select id from iso_answers where respondent_id = 5)
            or id in(select id from iso_answers where respondent_id = 5 and status = 0)');

            return view('result')
            ->with('result', $result->result)
            ->with('not_achive', $not_achive)
            ->with('category', $category)
            ->with('answers_no', $answers_no);
        }else {
            return Redirect::to('/');
        }
    }

    public function audit()
    {
        return view('audit');
    }

    public function simple()
    {
        return view('simple');
    }

    public function test()
    {
        return view('login');
    }

    public function level1_copy($id)
    {
        $check_child = DB::table('iso_category_level2')
        ->where('category_level1_id', $id)
        ->first();
        if ($check_child != null) {
            return Redirect::to('iso_category_level1')->with('error', 'Data Turunan Harus Kosong');
        }
        $data = DB::table('iso_category_level1')
        ->where('id', $id)
        ->first();

        $insert = DB::table('iso_category_level2')
        ->insert([
            'category_level1_id' => $id,
            'code' => $data->code,
            'title' => $data->title,
            'max_score' => $data->max_score,
            'is_visible' => 0,
            'sort' => 1
        ]);

        return Redirect::to('iso_category_level1')->with('success', 'Data Berhasil Disalin');
    }

    public function level2_copy(Request $request, $id)
    {
        $check_child = DB::table('iso_category_level3')
        ->where('category_level2_id', $id)
        ->first();
        if ($check_child != null) {
            return Redirect::to('iso_category_level2?id='.$request->input('id'))->with('error', 'Data Turunan Harus Kosong');
        }
        $data = DB::table('iso_category_level2')
        ->where('id', $id)
        ->first();

        $insert = DB::table('iso_category_level3')
        ->insert([
            'category_level2_id' => $id,
            'code' => $data->code,
            'title' => $data->title,
            'max_score' => $data->max_score,
            'is_visible' => $data->is_visible,
            'sort' => 1
        ]);

        return Redirect::to('iso_category_level2?id='.$request->input('id'))->with('success', 'Data Berhasil Disalin');
    }

    public function level3_copy(Request $request, $id)
    {
        $check_child = DB::table('iso_category_level4')
        ->where('category_level3_id', $id)
        ->first();
        if ($check_child != null) {
            return Redirect::to('iso_category_level3?id='.$request->input('id'))->with('error', 'Data Turunan Harus Kosong');
        }
        $data = DB::table('iso_category_level3')
        ->where('id', $id)
        ->first();

        $insert = DB::table('iso_category_level4')
        ->insert([
            'category_level3_id' => $id,
            'code' => $data->code,
            'title' => $data->title,
            'max_score' => $data->max_score,
            'is_visible' => $data->is_visible,
            'sort' => 1
        ]);

        return Redirect::to('iso_category_level3?id='.$request->input('id'))->with('success', 'Data Berhasil Disalin');
    }

    public function update_sort1(Request $request)
    {
        $get = DB::table('iso_category_level1')
        ->where('id', $request->input('id'))
        ->first();
        $check = DB::table('iso_category_level1')
        ->where('sort', $request->input('sort'))
        ->first();

        if ($check) {
            $update1 = DB::table('iso_category_level1')
            ->where('id', $check->id)
            ->update([
                'sort' => $get->sort
            ]);
        }
        $update = DB::table('iso_category_level1')
        ->where('id', $get->id)
        ->update([
            'sort' => $request->input('sort')
        ]);

        return back();
    }

    public function update_sort2(Request $request)
    {
        $get = DB::table('iso_category_level2')
        ->where('id', $request->input('id'))
        ->first();
        $check = DB::table('iso_category_level2')
        ->where('sort', $request->input('sort'))
        ->where('category_level1_id', $get->category_level1_id)
        ->first();

        if ($check) {
            $update1 = DB::table('iso_category_level2')
            ->where('id', $check->id)
            ->update([
                'sort' => $get->sort
            ]);
        }
        $update = DB::table('iso_category_level2')
        ->where('id', $get->id)
        ->update([
            'sort' => $request->input('sort')
        ]);

        return back();
    }

    public function update_sort3(Request $request)
    {
        $get = DB::table('iso_category_level3')
        ->where('id', $request->input('id'))
        ->first();
        $check = DB::table('iso_category_level3')
        ->where('sort', $request->input('sort'))
        ->where('category_level2_id', $get->category_level2_id)
        ->first();

        if ($check) {
            $update1 = DB::table('iso_category_level3')
            ->where('id', $check->id)
            ->update([
                'sort' => $get->sort
            ]);
        }
        $update = DB::table('iso_category_level3')
        ->where('id', $get->id)
        ->update([
            'sort' => $request->input('sort')
        ]);

        return back();
    }

    public function update_sort4(Request $request)
    {
        $get = DB::table('iso_category_level4')
        ->where('id', $request->input('id'))
        ->first();
        $check = DB::table('iso_category_level4')
        ->where('sort', $request->input('sort'))
        ->where('category_level3_id', $get->category_level3_id)
        ->first();

        if ($check) {
            $update1 = DB::table('iso_category_level4')
            ->where('id', $check->id)
            ->update([
                'sort' => $get->sort
            ]);
        }
        $update = DB::table('iso_category_level4')
        ->where('id', $get->id)
        ->update([
            'sort' => $request->input('sort')
        ]);

        return back();
    }

    public function update_sort_document_review(Request $request)
    {
        $get = DB::table('document_reviews')
        ->where('id', $request->input('id'))
        ->first();
        $check = DB::table('document_reviews')
        ->where('sort', $request->input('sort'))
        ->first();

        if ($check) {
            $update1 = DB::table('document_reviews')
            ->where('id', $check->id)
            ->update([
                'sort' => $get->sort
            ]);
        }
        $update = DB::table('document_reviews')
        ->where('id', $get->id)
        ->update([
            'sort' => $request->input('sort')
        ]);

        return back();
    }
}

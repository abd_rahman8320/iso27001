<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\DocumentReview;
use Illuminate\Support\Facades\Redirect;

class DocumentReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DocumentReview::orderBy('sort', 'asc')
        ->get();

        return view('document_review')
        ->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('document_review_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = $request->input('code');
        $description = $request->input('description');
        $is_mandatory = $request->input('is_mandatory');
        $sort = DocumentReview::max('sort');

        $data = new DocumentReview;
        $data->code = $code;
        $data->description = $description;
        $data->is_mandatory = $is_mandatory;
        $data->sort = $sort + 1;
        $data->save();

        return Redirect::to('document_review')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DocumentReview::find($id);

        return view('document_review_edit')
        ->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $code = $request->input('code');
        $description = $request->input('description');
        $is_mandatory = $request->input('is_mandatory');

        $data = DocumentReview::find($id);
        $data->code = $code;
        $data->description = $description;
        $data->is_mandatory = $is_mandatory;
        $data->save();

        return Redirect::to('document_review')->with('success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = DocumentReview::find($id);
        $data->delete();

        return Redirect::to('document_review')->with('success', 'Data Berhasil Dihapus');
    }
}

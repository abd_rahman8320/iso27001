<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\IsoCategoryLevel1;
use App\Http\Requests;
use DB;

class IsoCategoryLevel1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = IsoCategoryLevel1::orderBy('sort', 'asc')->get();
        $jumlah_bobot = DB::table('iso_category_level1')->sum('max_score');
        $consistency_ratio = DB::table('consistency_ratio_level1s')->first();

        return view('iso_category_level1')
        ->with('data', $data)
        ->with('jumlah_bobot', round($jumlah_bobot))
        ->with('consistency_ratio', $consistency_ratio);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('iso_category_level1_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->input('title');
        $code = $request->input('code');

        $sort = IsoCategoryLevel1::max('sort');

        $data = new IsoCategoryLevel1;
        $data->code = $code;
        $data->title = $title;
        $data->sort = $sort + 1;
        $data->save();

        return Redirect::to('iso_category_level1')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = IsoCategoryLevel1::find($id);

        return view('iso_category_level1_edit')
        ->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = $request->input('title');
        $code = $request->input('code');

        $data = IsoCategoryLevel1::find($id);
        $data->code = $code;
        $data->title = $title;
        $data->save();

        return Redirect::to('iso_category_level1')->with('success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = IsoCategoryLevel1::find($id);
        $check_child = DB::table('iso_category_level2')
        ->where('category_level1_id', $id)
        ->first();
        if ($check_child != null) {
            return Redirect::to('iso_category_level1')->with('error', 'Hapus Turunannya Terlebih Dahulu');
        }
        $data->delete();

        return Redirect::to('iso_category_level1')->with('success', 'Data Berhasil Dihapus');
    }
}

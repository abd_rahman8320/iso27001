<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Respondents;
use App\IsoQuestion;
use App\IsoAnswer;
use App\DocumentReviewAnswer;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

class AnswerController extends Controller
{
    public function iso_answer(Request $request)
    {
        $name = $request->input('name');
        $address = $request->input('address');

        $insert_respondent = new Respondents;
        $insert_respondent->name = $name;
        $insert_respondent->address = $address;
        $insert_respondent->save();

        $respondent = Respondents::orderBy('id', 'desc')->first();

        foreach ($request->input('question') as $question) {
            $respondent_id = $respondent->id;
            $question_id = $question['question_id'];
            $get_question = IsoQuestion::find($question_id);
            $max_score = $get_question->max_score;
            $status = $question['status'];
            if ($status == 1) {
                $score = $max_score / 2;
            }elseif ($status == 2) {
                $score = $max_score;
            }else {
                $score = 0;
            }

            $insert_answer = new IsoAnswer;
            $insert_answer->respondent_id = $respondent_id;
            $insert_answer->question_id = $question_id;
            $insert_answer->max_score = $max_score;
            $insert_answer->score = $score;
            $insert_answer->status = $status;
            $insert_answer->save();
        }

        return 'Success';
    }

    public function questioner()
    {
        return view('questioner');
    }

    public function check_next_question($level1, $level2, $level3, $level4)
    {
        $next_question = DB::table('iso_category_level4')
        ->leftJoin('iso_category_level3', 'iso_category_level3.id', '=', 'iso_category_level4.category_level3_id')
        ->leftJoin('iso_category_level2', 'iso_category_level2.id', '=', 'iso_category_level3.category_level2_id')
        ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
        ->where('iso_category_level4.sort', $level4)
        ->where('iso_category_level3.sort', $level3)
        ->where('iso_category_level2.sort', $level2)
        ->where('iso_category_level1.sort', $level1)
        ->select(
            'iso_category_level4.code as code4',
            'iso_category_level4.title as title4',
            'iso_category_level4.sort as sort4',
            'iso_category_level4.id as id4',
            'iso_category_level3.code as code3',
            'iso_category_level3.title as title3',
            'iso_category_level3.sort as sort3',
            'iso_category_level3.id as id3',
            'iso_category_level2.code as code2',
            'iso_category_level2.title as title2',
            'iso_category_level2.sort as sort2',
            'iso_category_level2.id as id2',
            'iso_category_level1.code as code1',
            'iso_category_level1.title as title1',
            'iso_category_level1.sort as sort1',
            'iso_category_level1.id as id1'
            )
        ->first();

        if ($next_question == null) {
            return null;
        }else {
            return $level1.'/'.$level2.'/'.$level3.'/'.$level4;
        }
    }

    public function check_previous_question($level4, $level3, $level2, $level1)
    {
        if ($level2 == 0) {
            $level2 = DB::table('iso_category_level2')
            ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
            ->where('iso_category_level1.sort', $level1)
            ->max('iso_category_level2.sort');
        }
        if ($level3 == 0) {
            $level3 = DB::table('iso_category_level3')
            ->leftJoin('iso_category_level2', 'iso_category_level2.id', '=', 'iso_category_level3.category_level2_id')
            ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
            ->where('iso_category_level2.sort', $level2)
            ->where('iso_category_level1.sort', $level1)
            ->max('iso_category_level3.sort');
        }
        if ($level4 == 0) {
            $level4 = DB::table('iso_category_level4')
            ->leftJoin('iso_category_level3', 'iso_category_level3.id', '=', 'iso_category_level4.category_level3_id')
            ->leftJoin('iso_category_level2', 'iso_category_level2.id', '=', 'iso_category_level3.category_level2_id')
            ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
            ->where('iso_category_level3.sort', $level3)
            ->where('iso_category_level2.sort', $level2)
            ->where('iso_category_level1.sort', $level1)
            ->max('iso_category_level4.sort');
        }

        return 'question/'.$level1.'/'.$level2.'/'.$level3.'/'.$level4;
    }

    public function question4($level1, $level2, $level3, $level4)
    {
        $question = DB::table('iso_category_level4')
        ->leftJoin('iso_category_level3', 'iso_category_level3.id', '=', 'iso_category_level4.category_level3_id')
        ->leftJoin('iso_category_level2', 'iso_category_level2.id', '=', 'iso_category_level3.category_level2_id')
        ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
        ->where('iso_category_level4.sort', $level4)
        ->where('iso_category_level3.sort', $level3)
        ->where('iso_category_level2.sort', $level2)
        ->where('iso_category_level1.sort', $level1)
        ->select(
            'iso_category_level4.code as code4',
            'iso_category_level4.title as title4',
            'iso_category_level4.sort as sort4',
            'iso_category_level4.id as id4',
            'iso_category_level3.code as code3',
            'iso_category_level3.title as title3',
            'iso_category_level3.sort as sort3',
            'iso_category_level3.id as id3',
            'iso_category_level2.code as code2',
            'iso_category_level2.title as title2',
            'iso_category_level2.sort as sort2',
            'iso_category_level2.id as id2',
            'iso_category_level1.code as code1',
            'iso_category_level1.title as title1',
            'iso_category_level1.sort as sort1',
            'iso_category_level1.id as id1'
            )
        ->first();

        $level4_prev = $level4 - 1;
        if ($level4_prev == 0) {
            $level3_prev = $level3 - 1;
            if ($level3_prev == 0) {
                $level2_prev = $level2 - 1;
                if ($level2_prev == 0) {
                    $level1_prev = $level1 - 1;
                    if ($level1_prev == 0) {
                        $previous_url = 'question/1/1/1/1';
                    }else {
                        $previous_url = self::check_previous_question($level4_prev, $level3_prev, $level2_prev, $level1_prev);
                    }
                }else {
                    $previous_url = self::check_previous_question($level4_prev, $level3_prev, $level2_prev, $level1);
                }
            }else {
                $previous_url = self::check_previous_question($level4_prev, $level3_prev, $level2, $level1);
            }
        }else {
            $previous_url = 'question/'.$level1.'/'.$level2.'/'.$level3.'/'.$level4_prev;
        }

        return view('question4')
        ->with('question', $question)
        ->with('level1_sort', $level1)
        ->with('level2_sort', $level2)
        ->with('level3_sort', $level3)
        ->with('level4_sort', $level4)
        ->with('previous_url', $previous_url);
    }

    public function next_question(Request $request)
    {
        if (Session::has('respondent_id')) {
            $question_id = $request->input('question_id');
            $answer = $request->input('answer');
            $level1 = $request->input('level1');
            $level2 = $request->input('level2');
            $level3 = $request->input('level3');
            $level4 = $request->input('level4');

            $check = DB::table('iso_answers')
            ->where('respondent_id', Session::get('respondent_id'))
            ->where('question_id', $question_id)
            ->first();

            if ($check) {
                $data = IsoAnswer::find($check->id);
                $data->score = $answer;
                $data->status = $answer;
                $data->save();
            }else {
                $get = DB::table('iso_category_level4')
                ->leftJoin('iso_category_level3', 'iso_category_level3.id', '=', 'iso_category_level4.category_level3_id')
                ->leftJoin('iso_category_level2', 'iso_category_level2.id', '=', 'iso_category_level3.category_level2_id')
                ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
                ->where('iso_category_level4.id', $question_id)
                ->selectRaw('
                    iso_category_level4.max_score * iso_category_level3.max_score * iso_category_level2.max_score * iso_category_level1.max_score as max_score
                ')
                ->first();

                $data = new IsoAnswer;
                $data->respondent_id = Session::get('respondent_id');
                $data->question_id = $question_id;
                $data->max_score = $get->max_score;
                $data->score = $answer;
                $data->status = $answer;
                $data->save();
            }

            $check = self::check_next_question($level1, $level2, $level3, $level4+1);
            if ($check == null) {
                $check = self::check_next_question($level1, $level2, $level3+1, 1);
                if ($check == null) {
                    $check = self::check_next_question($level1, $level2+1, 1, 1);
                    if ($check == null) {
                        $check = self::check_next_question($level1+1, 1, 1, 1);
                        if ($check == null) {
                            return Redirect::to('/')->with('success', 'Anda Telah Mengisi Semua Kuesioner');
                        }
                    }
                }
            }

            return Redirect::to('question/'.$check);

        }else {
            return Redirect::to('questioner');
        }

    }

    public function question3($level1, $level2, $level3)
    {
        $question = DB::table('iso_category_level4')
        ->leftJoin('iso_category_level3', 'iso_category_level3.id', '=', 'iso_category_level4.category_level3_id')
        ->leftJoin('iso_category_level2', 'iso_category_level2.id', '=', 'iso_category_level3.category_level2_id')
        ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
        ->where('iso_category_level3.sort', $level3)
        ->where('iso_category_level2.sort', $level2)
        ->where('iso_category_level1.sort', $level1)
        ->select(
            'iso_category_level4.code as code4',
            'iso_category_level4.title as title4',
            'iso_category_level4.sort as sort4',
            'iso_category_level3.code as code3',
            'iso_category_level3.title as title3',
            'iso_category_level3.sort as sort3',
            'iso_category_level3.id as id3',
            'iso_category_level2.code as code2',
            'iso_category_level2.title as title2',
            'iso_category_level2.sort as sort2',
            'iso_category_level1.code as code1',
            'iso_category_level1.title as title1',
            'iso_category_level1.sort as sort1'
            )
        ->get();

        if ($question->isEmpty()) {
            $get_id = DB::table('iso_category_level3')
            ->leftJoin('iso_category_level2', 'iso_category_level2.id', '=', 'iso_category_level3.category_level2_id')
            ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
            ->where('iso_category_level3.sort', $level3)
            ->where('iso_category_level2.sort', $level2)
            ->where('iso_category_level1.sort', $level1)
            ->first();

            return Redirect::to('iso_category_level4?id='.$get_id->id)->with('error', 'Pertanyaan Kosong');
        }

        return view('question3')
        ->with('question', $question)
        ->with('level3_sort', $level3)
        ->with('level2_sort', $level2)
        ->with('level1_sort', $level1);
    }

    public function question2($level1, $level2)
    {
        $question = DB::table('iso_category_level3')
        ->leftJoin('iso_category_level2', 'iso_category_level2.id', '=', 'iso_category_level3.category_level2_id')
        ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
        ->where('iso_category_level2.sort', $level2)
        ->where('iso_category_level1.sort', $level1)
        ->select(
            'iso_category_level3.code as code3',
            'iso_category_level3.title as title3',
            'iso_category_level3.sort as sort3',
            'iso_category_level2.code as code2',
            'iso_category_level2.title as title2',
            'iso_category_level2.sort as sort2',
            'iso_category_level1.code as code1',
            'iso_category_level1.title as title1',
            'iso_category_level1.sort as sort1'
            )
        ->get();

        if ($question->isEmpty()) {
            $get_id = DB::table('iso_category_level2')
            ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
            ->where('iso_category_level2.sort', $level2)
            ->where('iso_category_level1.sort', $level1)
            ->first();

            return Redirect::to('iso_category_level3?id='.$get_id->id)->with('error', 'Pertanyaan Kosong');
        }

        // dd($question);

        return view('question2')
        ->with('question', $question)
        ->with('level2_sort', $level2)
        ->with('level1_sort', $level1);
    }

    public function question1($level1)
    {
        $question = DB::table('iso_category_level2')
        ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
        ->where('iso_category_level1.sort', $level1)
        ->select(
            'iso_category_level2.code as code2',
            'iso_category_level2.title as title2',
            'iso_category_level2.sort as sort2',
            'iso_category_level1.code as code1',
            'iso_category_level1.title as title1',
            'iso_category_level1.sort as sort1'
            )
        ->get();

        if ($question->isEmpty()) {
            $get_id = DB::table('iso_category_level1')
            ->where('sort', $level1)
            ->first();

            return Redirect::to('iso_category_level2?id='.$get_id->id)->with('error', 'Pertanyaan Kosong');
        }

        // dd($question);

        return view('question1')
        ->with('question', $question)
        ->with('level1_sort', $level1);
    }

    public function question()
    {
        $question = DB::table('iso_category_level1')
        ->select(
            'iso_category_level1.code as code1',
            'iso_category_level1.title as title1',
            'iso_category_level1.sort as sort1'
            )
        ->get();

        if ($question->isEmpty()) {
            return Redirect::to('/')->with('error', 'Pertanyaan Kosong');
        }

        // dd($question);

        return view('question')
        ->with('question', $question);
    }

    public function input_respondent(Request $request)
    {
        $name = $request->input('name');
        $password = $request->input('password');
        $address = $request->input('address');
        $is_admin = $request->input('is_admin');
        if ($is_admin != 1) {
            $is_admin = 0;
        }

        $data = new Respondents;
        $data->name = $name;
        $data->address = $address;
        $data->password = $password;
        $data->is_admin = $is_admin;
        $data->save();

        return Redirect::to('questioner');
    }

    public function question_document()
    {
        $data = DB::table('document_reviews')
        ->get();

        return view('question_document')
        ->with('data', $data);
    }

    public function input_question_document(Request $request)
    {
        $id = $request->input('id');
        $answer = $request->input('answer');

        DocumentReviewAnswer::where('respondent_id', Session::get('respondent_id'))->delete();
        for ($i=0; $i < count($id); $i++) {
            $data = new DocumentReviewAnswer;
            $data->respondent_id = Session::get('respondent_id');
            $data->question_id = $id[$i];
            if (isset($answer[$i])) {
                $data->answer = $answer[$i];
            }else {
                $data->answer = 0;
            }
            $data->save();
        }

        return Redirect::to('question/1/1/1/1');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\IsoCategoryLevel1;
use App\IsoCategoryLevel2;
use App\IsoCategoryLevel3;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class IsoCategoryLevel3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = DB::table('iso_category_level3')
        ->where('category_level2_id', $request->input('id'))
        ->orderBy('sort', 'asc')
        ->get();

        $data_parent = DB::table('iso_category_level2')
        ->leftJoin('iso_category_level1', 'iso_category_level1.id', '=', 'iso_category_level2.category_level1_id')
        ->where('iso_category_level2.id', $request->input('id'))
        ->select('iso_category_level1.id as id1', 'iso_category_level1.code as code1', 'iso_category_level2.id as id2', 'iso_category_level2.code as code2')
        ->first();

        $jumlah_bobot = DB::table('iso_category_level3')
        ->where('category_level2_id', $request->input('id'))
        ->sum('max_score');

        $consistency_ratio = DB::table('consistency_ratio_level3s')
        ->where('parent_id', $request->input('id'))
        ->first();

        return view('iso_category_level3')
        ->with('data', $data)
        ->with('id2', $request->input('id'))
        ->with('id1', $data_parent->id1)
        ->with('code1', $data_parent->code1)
        ->with('code2', $data_parent->code2)
        ->with('jumlah_bobot', round($jumlah_bobot))
        ->with('consistency_ratio', $consistency_ratio);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('iso_category_level3_create')
        ->with('id', $request->input('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = $request->input('code');
        $category_level2_id = $request->input('category_level2_id');
        $title = $request->input('title');
        $is_visible = $request->input('is_visible');

        $sort = IsoCategoryLevel3::where('category_level2_id', $category_level2_id)->max('sort');

        $data = new IsoCategoryLevel3;
        $data->category_level2_id = $category_level2_id;
        $data->code = $code;
        $data->title = $title;
        $data->is_visible = $is_visible;
        $data->sort = $sort + 1;
        $data->save();

        return Redirect::to('iso_category_level3?id='.$category_level2_id)->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = IsoCategoryLevel3::find($id);

        return view('iso_category_level3_edit')
        ->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = IsoCategoryLevel3::find($id);
        $data->category_level2_id = $request->input('category_level2_id');
        $data->code = $request->input('code');
        $data->title = $request->input('title');
        $data->is_visible = $request->input('is_visible');
        $data->save();

        return Redirect::to('iso_category_level3?id='.$request->input('category_level2_id'))->with('success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = IsoCategoryLevel3::find($id);
        $category_level2_id = $data->category_level2_id;
        $check_child = DB::table('iso_category_level4')
        ->where('category_level3_id', $id)
        ->first();
        if ($check_child != null) {
            return Redirect::to('iso_category_level3?id='.$category_level2_id)->with('error', 'Hapus Turunannya Terlebih Dahulu');
        }
        $data->delete();

        return Redirect::to('iso_category_level3?id='.$category_level2_id)->with('success', 'Data Berhasil Dihapus');
    }
}

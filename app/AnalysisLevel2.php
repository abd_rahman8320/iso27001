<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalysisLevel2 extends Model
{
    protected $table = 'analysis_level2s';

    protected $fillable = [
        'first_criteria',
        'second_criteria',
        'parent_id',
        'scale',
        'result'
    ];
}

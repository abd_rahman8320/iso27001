<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalysisLevel4 extends Model
{
    protected $table = 'analysis_level4s';

    protected $fillable = [
        'first_criteria',
        'second_criteria',
        'parent_id',
        'scale',
        'result'
    ];
}

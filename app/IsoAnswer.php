<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IsoAnswer extends Model
{
    protected $table = 'iso_answers';

    protected $fillable = [
        'respondent_id',
        'question_id',
        'max_score',
        'score',
        'status'
    ];
}

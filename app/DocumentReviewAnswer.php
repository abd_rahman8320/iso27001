<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentReviewAnswer extends Model
{
    protected $table = 'document_review_answers';

    protected $fillable = [
        'respondent_id',
        'question_id',
        'answer'
    ];
}

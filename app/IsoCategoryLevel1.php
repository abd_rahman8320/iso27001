<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IsoCategoryLevel1 extends Model
{
    protected $table = 'iso_category_level1';

    protected $fillable = [
        'code',
        'title',
        'sum',
        'max_score',
        'sort'
    ];
}

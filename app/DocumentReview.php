<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentReview extends Model
{
    protected $table = 'document_reviews';

    protected $fillable = [
        'code',
        'description',
        'is_mandatory'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsistencyRatioLevel3 extends Model
{
    protected $table = 'consistency_ratio_level3s';

    protected $fillable = [
        'parent_id',
        'n',
        'eigen_max',
        'ci',
        'cr'
    ];
}

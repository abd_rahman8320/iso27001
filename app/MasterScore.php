<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterScore extends Model
{
    protected $table = 'master_scores';

    protected $fillable = [
        'score',
        'keterangan'
    ];
}

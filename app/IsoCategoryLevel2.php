<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IsoCategoryLevel2 extends Model
{
    protected $table = 'iso_category_level2';

    protected $fillable = [
        'category_level1_id',
        'code',
        'title',
        'sum',
        'max_score',
        'is_visible',
        'sort'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respondents extends Model
{
    protected $table = 'respondents';

    protected $fillable = [
        'name',
        'address',
        'password',
        'is_admin'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalysisLevel3 extends Model
{
    protected $table = 'analysis_level3s';

    protected $fillable = [
        'first_criteria',
        'second_criteria',
        'parent_id',
        'scale',
        'result'
    ];
}

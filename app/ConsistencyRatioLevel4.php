<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsistencyRatioLevel4 extends Model
{
    protected $table = 'consistency_ratio_level4s';

    protected $fillable = [
        'parent_id',
        'n',
        'eigen_max',
        'ci',
        'cr'
    ];
}

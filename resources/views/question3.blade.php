<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="{{url('css/simple-sidebar.css')}}" rel="stylesheet">

    <!-- breadcrumb -->
    <link rel="stylesheet" href="{{url('css/breadcrumb.css')}}">

    <link rel="stylesheet" href="{{url('css/app.css')}}">

</head>

<body>

    <div id="wrapper">

        @include('layouts.sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <h1>Questioner</h1>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::get('error'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('error')}}
                </div>
                @endif
                <br>
                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <ul class="breadcrumb">
                            <li><a href="{{url('question')}}">Root</a></li>
                            <li><a href="{{url('question')}}/{{$question[0]->sort1}}">{{$question[0]->code1}}</a></li>
                            <li><a href="{{url('question')}}/{{$question[0]->sort1}}/{{$question[0]->sort2}}">{{$question[0]->code2}}</a></li>
                            <li>{{$question[0]->code3}}</li>
                        </ul>
                    </div>
                </div>

                <p><b>Pilih Kode Pertanyaan Di Bawah Ini</b></p>
                <table border="0">
                    @foreach($question as $q)
                    <tr>
                        <td><a href="{{url('question')}}/{{$q->sort1}}/{{$q->sort2}}/{{$q->sort3}}/{{$q->sort4}}">{{$q->code4}}</a></td>
                    </tr>
                    @endforeach
                </table>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle" style="display:none;">Toggle Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.bundle.min.js')}}"></script>

    <!-- Menu Toggle Script -->
    <script>
    $(document).ready(function(e){
        $('#menu-toggle').click();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>

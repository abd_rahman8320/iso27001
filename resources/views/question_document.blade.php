<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="{{url('css/simple-sidebar.css')}}" rel="stylesheet">

    <!-- breadcrumb -->
    <link rel="stylesheet" href="{{url('css/breadcrumb.css')}}">

    <link rel="stylesheet" href="{{url('css/app.css')}}">

</head>

<body>

    <div id="wrapper">

        @include('layouts.sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <h1>Questioner</h1>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::get('error'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('error')}}
                </div>
                @endif
                <br>
                <form class="form-horizontal" action="{{url('input_question_document')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <table class="table table-striped">
                        <tr>
                            <th>Kode</th>
                            <th>Pertanyaan</th>
                            <th>Jawaban</th>
                        </tr>
                        <?php $i=0; ?>
                        @foreach($data as $d)
                        <tr>
                            <td>{{$d->code}}</td>
                            <td>{{$d->description}}</td>
                            <td>
                                <input type="hidden" name="id[{{$i}}]" value="{{$d->id}}">
                                <div class="checkbox">
                                    @if($d->is_mandatory == 1)
                                    <label for=""><input type="checkbox" name="answer[{{$i}}]" value="1" required oninput="setCustomValidity('')" oninvalid="this.setCustomValidity('Dokumen Harus Ada')"> Tersedia</label>
                                    @else
                                    <label for=""><input type="checkbox" name="answer[{{$i}}]" value="1"> Tersedia</label>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </table>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary col-lg-offset-10" name="button">Submit</button>
                    </div>
                </form>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle" style="display:none;">Toggle Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.bundle.min.js')}}"></script>

    <!-- Menu Toggle Script -->
    <script>
    $(document).ready(function(e){
        $('#menu-toggle').click();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>

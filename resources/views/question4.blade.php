<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="{{url('css/simple-sidebar.css')}}" rel="stylesheet">

    <!-- breadcrumb -->
    <link rel="stylesheet" href="{{url('css/breadcrumb.css')}}">

    <link rel="stylesheet" href="{{url('css/app.css')}}">

</head>

<body>

    <div id="wrapper">

        @include('layouts.sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <h1>Questioner</h1>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::get('error'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('error')}}
                </div>
                @endif
                <br>
                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <ul class="breadcrumb">
                            <li><a href="{{url('question')}}">Root</a></li>
                            <li><a href="{{url('question')}}/{{$question->sort1}}">{{$question->code1}}</a></li>
                            <li><a href="{{url('question')}}/{{$question->sort1}}/{{$question->sort2}}">{{$question->code2}}</a></li>
                            <li><a href="{{url('question')}}/{{$question->sort1}}/{{$question->sort2}}/{{$question->sort3}}">{{$question->code3}}</a></li>
                            <li>{{$question->code4}}</li>
                        </ul>
                    </div>
                </div>

                <table border="0">
                    <tr>
                        <td><b>Bagian &nbsp;</b></td>
                        <td><b>: &nbsp;</b></td>
                        <td><b>{{$question->code1}} {{$question->title1}}</b></td>
                    </tr>
                    <tr>
                        <td><b>Sub-Bagian I &nbsp;</b></td>
                        <td><b>: &nbsp;</b></td>
                        <td><b>{{$question->code2}} {{$question->title2}}</b></td>
                    </tr>
                    <tr>
                        <td><b>Sub-Bagian II &nbsp;</b></td>
                        <td><b>: &nbsp;</b></td>
                        <td><b>{{$question->code3}} {{$question->title3}}</b></td>
                    </tr>
                </table>

                <!-- <p><b>Bagian: {{$question->code1}}. {{$question->title1}}</b></p>
                <p><b>Sub-Bagian I: {{$question->code2}}. {{$question->title2}}</b></p>
                <p><b>Sub-Bagian II: {{$question->code3}}. {{$question->title3}}</b></p> -->
                <br>
                <p><b>Pertanyaan:</b></p>
                <form class="form-horizontal" action="{{url('next_question')}}" method="post">
                    <input type="hidden" name="question_id" value="{{$question->id4}}">
                    <input type="hidden" name="level1" value="{{$level1_sort}}">
                    <input type="hidden" name="level2" value="{{$level2_sort}}">
                    <input type="hidden" name="level3" value="{{$level3_sort}}">
                    <input type="hidden" name="level4" value="{{$level4_sort}}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label for="" class="control-label col-lg-6" style="text-align:left">{{$question->title4}}</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="answer">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        @if($level1_sort == 1 && $level2_sort == 1 && $level3_sort == 1 && $level4_sort == 1)
                        @else
                        <a href="{{url($previous_url)}}"><button type="button" class="btn btn-primary" name="button">Previous</button></a>
                        @endif
                        <button type="submit" class="btn btn-primary col-lg-offset-10" name="button">Next</button>
                    </div>
                </form>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle" style="display:none;">Toggle Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.bundle.min.js')}}"></script>

    <!-- Menu Toggle Script -->
    <script>
    $(document).ready(function(e){
        $('#menu-toggle').click();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>

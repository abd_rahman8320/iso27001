<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/simple-sidebar.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/app.css')}}">

</head>

<body>

    <div id="wrapper">

        @include('layouts.sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <h1>LEVEL I CREATE</h1>
                <form class="form-horizontal" action="{{url('iso_category_level1')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label for="code" class="control-label col-lg-1">Kode</label>
                        <div class="col-lg-10">
                            <input type="text" name="code" value="" class="form-control" id="code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="control-label col-lg-1">Title</label>
                        <div class="col-lg-10">
                            <input type="text" name="title" value="" class="form-control" id="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="button" class="btn btn-success">Submit</button>
                        <a href="{{url('iso_category_level1')}}"><button type="button" name="button" class="btn btn-default">Cancel</button></a>
                    </div>
                </form>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle" style="display:none;">Toggle Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.bundle.min.js')}}"></script>

    <!-- Menu Toggle Script -->
    <script>
    $(document).ready(function(e){
        $('#menu-toggle').click();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="css/simple-sidebar.css" rel="stylesheet">

    <!-- breadcrumb -->
    <link rel="stylesheet" href="css/breadcrumb.css">

    <link rel="stylesheet" href="{{url('css/app.css')}}">

</head>

<body>

    <div id="wrapper">

        @include('layouts.sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <h1>Create Respondent</h1>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::get('error'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('error')}}
                </div>
                @endif
                <br>
                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <ul class="breadcrumb">
                            Profile
                        </ul>
                    </div>
                </div>

                <form class="form-horizontal" action="{{url('input_respondent')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label for="" class="control-label col-lg-1">Username</label>
                        <div class="col-lg-10">
                            <input class="form-control" type="text" name="name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-lg-1">Password</label>
                        <div class="col-lg-10">
                            <input class="form-control" type="password" name="password" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-lg-1">Alamat</label>
                        <div class="col-lg-10">
                            <input class="form-control" type="text" name="address" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-lg-1">Admin</label>
                        <label for="" class="col-lg-3"><input type="checkbox" name="is_admin" value="1"> Yes</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-success col-lg-offset-1">Submit</button>
                    </div>
                </form>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle" style="display:none;">Toggle Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $(document).ready(function(e){
        $('#menu-toggle').click();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>

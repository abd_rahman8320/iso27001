<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="{{url('css/simple-sidebar.css')}}" rel="stylesheet">

    <!-- breadcrumb -->
    <link rel="stylesheet" href="{{url('css/breadcrumb.css')}}">

    <link rel="stylesheet" href="{{url('css/app.css')}}">

</head>

<body>

    <div id="wrapper">

        @include('layouts.sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <h1>Result</h1>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::get('error'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('error')}}
                </div>
                @endif
                <br>
                <br>

                <div class="row">
                    <div class="col-lg-6">
                        <canvas id="pieChart"></canvas>
                    </div>
                    <div class="col-lg-6">
                        <table class="table table-bordered">
                            <tr>
                                <th>Skor</th>
                                <th>{{number_format($result*100, 2)}}%</th>
                            </tr>
                            <tr>
                                <th>Kategori</th>
                                <th>{{$category}}</th>
                            </tr>
                        </table>
                    </div>
                </div>

                <br>
                <b>SYARAT YANG BELUM TERPENUHI :</b>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Pertanyaan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($answers_no as $answer_no)
                                    <tr>
                                        <td>{{$answer_no->code}}</td>
                                        <td>{{$answer_no->title}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle" style="display:none;">Toggle Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{url('js/chart.js')}}"></script>
    <!-- Menu Toggle Script -->
    <script>
    $(document).ready(function(e){
        $('#menu-toggle').click();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

    <script>
    //pie
    var ctxP = document.getElementById("pieChart").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
            labels: ["Score", "Belum Tercapai"],
            datasets: [{
                data: [{{$result}}, {{$not_achive}}],
                backgroundColor: ["#64991e", "#cf2e2e"],
                hoverBackgroundColor: ["#507a18", "#902020"]
            }]
        },
        options: {
            responsive: true
        }
    });

</script>

</body>

</html>

<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="#">
                Start Bootstrap
            </a>
        </li>
        <!-- <li>
            <a href="javascript:;" data-toggle="collapse" data-target="#categories"> Categories <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="categories" class="collapse">
                <li>
                    <a href="{{url('categories/create')}}">Add Category</a>
                </li>
                <li>
                    <a href="{{url('categories')}}">Manage Categories</a>
                </li>
            </ul>
        </li> -->
        @if(Session::get('respondent_is_admin') == 1)
        <li>
            <a href="{{url('iso_category_level1')}}">ISO 27001</a>
        </li>
        <li>
            <a href="{{url('document_review')}}">Document Review</a>
        </li>
        <li>
            <a href="{{url('questioner')}}">Create Respondent</a>
        </li>
        @endif
        <li>
            <a href="{{url('question_document')}}">Questioner</a>
        </li>
        <li>
            <a href="{{url('result')}}">Result</a>
        </li>
        <li>
            <a href="{{url('logout')}}">Log Out</a>
        </li>
    </ul>
</div>
<!-- /#sidebar-wrapper -->

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/simple-sidebar.css')}}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="{{url('css/app.css')}}"> -->

</head>

<body>

    <div id="wrapper">

        @include('layouts.sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <h1>JUDGEMENT SCALE LEVEL IV</h1>
                <form class="form-horizontal" action="{{url('analysis_level4s')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="parent_id" value="{{$id}}">
                    <?php $j=0; ?>
                    @foreach($x as $row_x)
                        @foreach($y[$j] as $row_y)
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <textarea class="form-control" name="name" rows="3" cols="" readonly>{{$row_x->code}} {{$row_x->title}}</textarea>
                                    <!-- <input type="text" class="form-control" value="{{$row_x->code}} {{$row_x->title}}" readonly> -->
                                    <input type="hidden" name="x[]" value="{{$row_x->id}}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select class="form-control" name="nilai[]">
                                        @foreach($scores as $score)
                                        <option value="{{$score->score}}">{{$score->score}} - {{$score->keterangan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <textarea class="form-control" name="name" rows="3" cols="" readonly>{{$row_y->code}} {{$row_y->title}}</textarea>
                                    <!-- <input type="text" class="form-control" value="{{$row_y->code}} {{$row_y->title}}" readonly> -->
                                    <input type="hidden" name="y[]" value="{{$row_y->id}}">
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <?php $j++; ?>
                    @endforeach
                    <div class="form-group">
                        <button type="submit" name="button" class="btn btn-success">Submit</button>
                        <a href="{{url('iso_category_level4')}}?id={{$id}}"><button type="button" name="button" class="btn btn-default">Cancel</button></a>
                    </div>
                </form>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle" style="display:none;">Toggle Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.bundle.min.js')}}"></script>

    <!-- Menu Toggle Script -->
    <script>
    $(document).ready(function(e){
        $('#menu-toggle').click();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>

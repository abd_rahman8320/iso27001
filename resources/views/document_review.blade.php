<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/simple-sidebar.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        @include('layouts.sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <h1>DOCUMENT REVIEW</h1>
                <br>
                @if(Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::get('error'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('error')}}
                </div>
                @endif

                <a href="{{url('document_review/create')}}"><button type="button" name="button" class="btn btn-info">Add</button></a>
                <br>
                <br>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>Kode</td>
                            <td>Deskripsi</td>
                            <td>Apakah Wajib?</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                        <tr>
                            <td style="width:100px">
                                <form class="" action="{{url('update_sort_document_review')}}" method="post" id="form-sort">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="id" value="{{$d->id}}">
                                    <input type="number" name="sort" value="{{$d->sort}}" class="form-control" id="sort">
                                </form>
                            </td>
                            <td>{{$d->code}}</td>
                            <td>{{$d->description}}</td>
                            <td>{{$d->is_mandatory}}</td>
                            <td>
                                <a href="{{url('document_review')}}/{{$d->id}}/edit"><button type="button" name="button" class="btn btn-info">Edit</button></a>
                                <form class="" action="{{url('document_review')}}/{{$d->id}}" method="post" style="display:inline">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" name="button" class="btn btn-danger">Delete</button>
                                </form>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle" style="display:none;">Toggle Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $(document).ready(function(e){
        $('#menu-toggle').click();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $('#sort').onkeypress(function(event){
        var x = event.which;
        if(x==13){
            document.getElementById("form-sort").submit();
        }
    });
    </script>

</body>

</html>
